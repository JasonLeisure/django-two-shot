from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Receipt, Account, ExpenseCategory
from .forms import ReceiptForm


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(
        request, "receipts/receipt_list.html", {"receipts": receipts}
    )


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    return render(request, "receipts/create_receipt.html", {"form": form})


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    return render(
        request, "receipts/accounts_list.html", {"accounts": accounts}
    )


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    return render(request, "", {"categories": categories})
